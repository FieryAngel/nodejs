const express = require('express');
const fs = require('fs');
const path = require('path');

const router = express.Router();

router.post('/', (req, res) => {
  const { filename, content } = req.body;

  if (fs.readdirSync('./files').includes(filename)) {
    return res.status(400).json({ message: `File '${filename}' already exist` });
  }

  if (!(/[A-Za-z0-9\.]\.(log|txt|json|yaml|xml|js)$/i).test(filename)) {
    return res.status(400).json({ message: 'File name or extension error' });
  }

  if (!content) {
    return res.status(400).json({ message: 'Please specify \'content\' parameter' });
  }

  fs.writeFile(`files/${filename}`, content, err => {
    if (err) {
      console.log(err);
    }

    console.log(`File ${filename} created`);
  });

  res.status(200).json({ message: `File ${filename} created successfully` });
});

router.get('/', function (req, res) {
  fs.readdir('./files/', (err, files) => {
    if (err) {
      return res.status(400).json({ message: 'Client error' });
    }

    res.status(200).json({ message: 'Success', files: files });
  });
});

router.get('/:filename', (req, res) => {
  const filename = req.params.filename;

  if (!fs.readdirSync('./files').includes(filename)) {
    return res.status(400).json({ message: `No file with '${filename}' filename found` });
  }

  const ext = path.extname(filename).slice(1);

  fs.readFile(`./files/${filename}`, (error, file) => {
    if (error) {
      console.log(`Error: ${error}`);
    }

    fs.stat(`./files/${filename}`, (err, stats) => {
      if (error) {
        console.log(`Error: ${error}`);
      }

      const { birthtime } = stats;

      res.status(200).json({ message: 'Success', filename: filename, content: file.toString(), extension: ext, uploadedDate: birthtime });
    });
  });
});

router.delete('/:filename', (req, res) => {
  const filename = req.params.filename;

  fs.unlink(`./files/${filename}`, (error, file) => {
    if (error) {
      return res.status(404).json({ message: `File ${filename} does not exist` });
    }

    res.status(200).json({ message: `File ${filename} was successfully deleted` });
  });
});

router.put('/:filename', (req, res) => {
  const filename = req.params.filename;
  const { content } = req.body;

  fs.writeFile(`files/${filename}`, content, { flag: 'r+' }, (error, file) => {
    if (error) {
      return res.status(400).json({ message: `File ${filename} does not exist` });
    }

    res.status(200).json({ message: `File ${filename} was successfully modified` });
  });
});

module.exports = router;