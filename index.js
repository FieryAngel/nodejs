const express = require('express');
const morgan = require('morgan');
const fs = require('fs');

const app = express();
const port = 8080;

const fileRouter = require('./fileRouter');

fs.access('./files', error => {
  if (error) {
    fs.mkdir('./files', err => {
      if (err) {
        console.log(err);
      } else {
        console.log('New directory successfully created.');
      }
    });
  }
});

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/files', fileRouter);

app.get('/', (req, res) => {
  res.send('Homework 1');
});

app.listen(port, () => {
  console.log(`Server works at http://localhost:${port}`);
});

